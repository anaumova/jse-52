package ru.tsc.anaumova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.anaumova.tm.api.service.IConnectionService;
import ru.tsc.anaumova.tm.api.service.IPropertyService;
import ru.tsc.anaumova.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.anaumova.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.tsc.anaumova.tm.api.service.dto.ITaskServiceDTO;
import ru.tsc.anaumova.tm.api.service.dto.IUserServiceDTO;
import ru.tsc.anaumova.tm.dto.model.ProjectDTO;
import ru.tsc.anaumova.tm.dto.model.TaskDTO;
import ru.tsc.anaumova.tm.dto.model.UserDTO;
import ru.tsc.anaumova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.anaumova.tm.exception.field.EmptyIdException;
import ru.tsc.anaumova.tm.exception.field.EmptyUserIdException;
import ru.tsc.anaumova.tm.service.dto.ProjectServiceDTO;
import ru.tsc.anaumova.tm.service.dto.ProjectTaskServiceDTO;
import ru.tsc.anaumova.tm.service.dto.TaskServiceDTO;
import ru.tsc.anaumova.tm.service.dto.UserServiceDTO;

public class ProjectTaskServiceTest {

    @NotNull
    private IProjectTaskServiceDTO projectTaskService;

    @NotNull
    private IUserServiceDTO userService;

    @NotNull
    private IProjectServiceDTO projectService;

    @NotNull
    private ITaskServiceDTO taskService;

    private String USER_ID;

    private String PROJECT_ID;

    private String TASK_ID;

    @Before
    public void init() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        projectTaskService = new ProjectTaskServiceDTO(connectionService);
        userService = new UserServiceDTO(connectionService, propertyService);
        projectService = new ProjectServiceDTO(connectionService);
        taskService = new TaskServiceDTO(connectionService);
        @NotNull final UserDTO user = userService.create("user", "user");
        USER_ID = user.getId();
        @NotNull final ProjectDTO project = projectService.create(USER_ID, "project");
        PROJECT_ID = project.getId();
        @NotNull final TaskDTO task = taskService.create(USER_ID, "task");
        TASK_ID = task.getId();
    }

    @After
    public void end() {
        taskService.clear(USER_ID);
        projectService.clear(USER_ID);
        userService.removeByLogin("user");
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertThrows(EmptyUserIdException.class,
                () -> projectTaskService.bindTaskToProject("", PROJECT_ID, TASK_ID));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID, "", TASK_ID));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID, PROJECT_ID, ""));
        Assert.assertThrows(TaskNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID, PROJECT_ID, "not_task_id"));

        projectTaskService.bindTaskToProject(USER_ID, PROJECT_ID, TASK_ID);
        @NotNull final TaskDTO task = taskService.findOneById(TASK_ID);
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(PROJECT_ID, task.getProjectId());
    }

    @Test
    public void unbindTaskFromProject() {
        Assert.assertThrows(EmptyUserIdException.class,
                () -> projectTaskService.unbindTaskFromProject("", PROJECT_ID, TASK_ID));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID, "", TASK_ID));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID, PROJECT_ID, ""));
        Assert.assertThrows(TaskNotFoundException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID, PROJECT_ID, "not_task_id"));
        projectTaskService.unbindTaskFromProject(USER_ID, PROJECT_ID, TASK_ID);
        @NotNull final TaskDTO task = taskService.findOneById(TASK_ID);
        Assert.assertNull(task.getProjectId());
    }

}