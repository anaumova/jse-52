package ru.tsc.anaumova.tm.api.repository.dto;

import ru.tsc.anaumova.tm.dto.model.ProjectDTO;

public interface IProjectRepositoryDTO extends IUserOwnedRepositoryDTO<ProjectDTO> {
}
